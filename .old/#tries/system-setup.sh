#!/bin/sh

show_welcome() {
    whiptail --title "SYSTEM SETUP" --msgbox "Let's set up your server system" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH
    show_menu_main
}

show_menu_main() {
    local CHOICE=$(whiptail --notags --cancel-button "Exit" --title "Main menu" --menu "" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH 4 \
        "1" "Add administrators" \
        "2" "Credits" \
        3>&1 1>&2 2>&3)
    case $CHOICE in
        1)
            add_admin
        ;;
        2)
            show_credits
        ;;
        *)
            exit
        ;;
    esac
}

add_admin() {
    local USERNAME=$(whiptail --nocancel --title "Username" --inputbox "Username:" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH 3>&1 1>&2 2>&3)

    local PASSWORD=$(whiptail --nocancel --title "Password" --passwordbox "Password:" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH 3>&1 1>&2 2>&3)
    local PASSWORD_CONFIRMATION=$(whiptail --nocancel --title "Password" --passwordbox "Password:" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH 3>&1 1>&2 2>&3)

    while ! [ $PASSWORD = $PASSWORD_CONFIRMATION ]
    do
        whiptail --title "Incorrect password" --msgbox "The passwords do not match, please try again." $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH
        local PASSWORD=$(whiptail --nocancel --title "Password" --passwordbox "Password:" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH 3>&1 1>&2 2>&3)
        local PASSWORD_CONFIRMATION=$(whiptail --nocancel --title "Password" --passwordbox "Password:" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH 3>&1 1>&2 2>&3)
    done

    echo "YEES"
    echo "PASSWORD=${PASSWORD}"
    echo "PASSwORD_CONFIRMATION=${PASSWORD_CONFIRMATION}"

    if (whiptail --title "Confirmation" --yesno "A login user '${USERNAME}' will be created with admin rigths. Are you sure you want to continue ?" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH) 
    then

        useradd $USERNAME
        echo -e "${PASSWORD}\n${PASSWORD}" | passwd $USERNAME
        useradd test
        sh -c "(echo -e "test\ntest" | passwd test)"

	    if (whiptail --title "Success" --yesno "${USERNAME} has been added to admin users. Would you like to add another admin user ?" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH)
        then
            add_admin
        else
            show_menu_main
        fi
    else
	    if (whiptail --title "Cancelled" --yesno "No admin user has been added. Would you like to add another admin user ?" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH)
        then
            add_admin
        else
            show_menu_main
        fi
    fi
}

WHIPTAIL_HEIGTH=15
WHIPTAIL_WIDTH=60

show_welcome