#!/bin/sh

# yum -y install \
# 	git \
# 	zsh \
# 	vim \
# 	bind-utils

usermod -s /bin/zsh root
usermod -s /bin/zsh $(logname)

sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="risto"/g' /root/.zshrc



show_welcome() {
    whiptail --title "SETUP OS CENTOS DEVELOPMENT SERVER" --msgbox "Welcome on the setup tool for centos development server" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH
    show_menu_main
}

show_menu_main() {	
	whiptail --title "SETUP OS CENTOS DEVELOPMENT SERVER" --msgbox "Welcome on the setup tool for centos development server" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH
}

show_welcome