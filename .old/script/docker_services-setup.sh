#!/bin/sh

create_configuration_files () {
    echo "create_configuration_files"

    # # mariadb
    # mkdir $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/mariadb
    # touch $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/mariadb/mariadb.env
    # echo 'MYSQL_ROOT_PASSWORD=password' >> $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/mariadb/mariadb.env

    # # mongo
    # mkdir $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/mongo
    # touch $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/mongo/mongo.env
    # echo 'MONGO_INITDB_ROOT_USERNAME=admin
    # MONGO_INITDB_ROOT_PASSWORD=password' >> $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/mongo/mongo.env


    # # mongo-express
    # mkdir $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/mongo-express
    # touch $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/mongo-express/mongo-express.env
    # echo 'ME_CONFIG_MONGODB_ADMINUSERNAME=admin
    # ME_CONFIG_MONGODB_ADMINPASSWORD=password
    # ME_CONFIG_BASICAUTH_USERNAME=user
    # ME_CONFIG_BASICAUTH_PASSWORD=password' >> $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/mongo-express/mongo-express.env

    # # postgresql
    # mkdir $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/postgresql
    # touch $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/postgresql/postgresql.env
    # echo "POSTGRES_USER=admin
    # POSTGRES_PASSWORD=password" >> $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/postgresql/postgresql.env

    # # pgadmin
    # mkdir $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/pgadmin
    # touch $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/pgadmin/pgadmin.env
    # echo "PGADMIN_DEFAULT_EMAIL=admin@admin.com ${As}
    # PGADMIN_DEFAULT_PASSWORD=password" >> $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/pgadmin/pgadmin.env
}

setup_dockerapp() {
    echo "setup_dockerapp"
#     mkdir -p $DOCKERAPP_ROOT

#     yum install -y docker yum-utils device-mapper-persistent-data lvm2

#     systemctl enable docker

#     systemctl start docker

#     curl -L https://github.com/docker/compose/releases/download/1.25.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

#     chmod +x /usr/local/bin/docker-compose

#     ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose

#     git clone https://gitlab.com/realit-company/setup-services-docker-development_server.git $DOCKERAPP_ROOT

#     if [ ! -f $DOCKERAPP_ROOT/.gitignore ]; then
#         touch $DOCKERAPP_ROOT/.gitignore
#     fi
#     echo "config-production/" >> $DOCKERAPP_ROOT/.gitignore

#     cp -r $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER $DOCKERAPP_ROOT/$PRODUCTION_CONFIG_FOLDER

#     chmod +x $DOCKERAPP_ROOT/install.sh
#     $DOCKERAPP_ROOT/install.sh $DOCKERAPP_ROOT

#     rm -rf $DOCKERAPP_ROOT/config-production
}

# SCRIPT_ROOT=$PWD
# PRODUCTION_CONFIG_FOLDER=config-production
# DOCKERAPP_ROOT=/dockerapp/work-application-server

# yum -y update

# yum -y install dialog

show_dialog_for_docker() {
    CONTAINER_NAME=$1
     
    MYSQL_ROOT_PASSWORD=""

    exec 3>&1
    VALUES=$(
        dialog --ok-label "Submit" \
	    --backtitle "Linux User Managment" \
	    --title "MariaDB" \
        15 50 0 \
	    "Username:" 1 1	"$user" 	1 10 10 0 \
        2>&1 1>&3)




    mkdir -p $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/mariadb
    touch $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/mariadb/mariadb.env
    echo 'MYSQL_ROOT_PASSWORD=password' >> $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER/mariadb/mariadb.env
}



DIALOG_WIDTH=100
DIALOG_HEIGTH=20


# if [ -d $SCRIPT_ROOT/$PRODUCTION_CONFIG_FOLDER ]; 
# then
#   setup_dockerapp()
# else
#   create_configuration_files()
# fi

