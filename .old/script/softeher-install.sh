#!/bin/sh

SCRIPT_ROOT=$PWD

yum -y groupinstall "Development Tools"

yum -y install wget

SOFTETHER_INSTALLER_ROOT=~/.tmp_softether_install
if [ -d $SOFTETHER_INSTALLER_ROOT ]; then
  rm -rf $SOFTETHER_INSTALLER_ROOT
fi

mkdir $SOFTETHER_INSTALLER_ROOT

wget https://github.com/SoftEtherVPN/SoftEtherVPN_Stable/releases/download/v4.29-9680-rtm/softether-vpnserver-v4.29-9680-rtm-2019.02.28-linux-x64-64bit.tar.gz -O $SOFTETHER_INSTALLER_ROOT/softether-vpnserver-linux.tar.gz

tar -xf $SOFTETHER_INSTALLER_ROOT/softether-vpnserver-linux.tar.gz --directory $SOFTETHER_INSTALLER_ROOT

rm $SOFTETHER_INSTALLER_ROOT/softether-vpnserver-linux.tar.gz

cd $SOFTETHER_INSTALLER_ROOT/vpnserver

yes "1" | make

cd $SCRIPT_ROOT

mv $SOFTETHER_INSTALLER_ROOT/vpnserver /usr/local

chmod 600 /usr/local/vpnserver/*

chmod 700 /usr/local/vpnserver/vpncmd

chmod 700 /usr/local/vpnserver/vpnserver

INITD_FILE=/etc/init.d/vpnserver
if [ -f $INITD_FILE ]; then
  rm $INITD_FILE
fi

touch $SOFTETHER_INSTALLER_ROOT/vpnserver

echo '#!/bin/sh
# chkconfig: 2345 99 01
# description: SoftEther VPN Server
DAEMON=/usr/local/vpnserver/vpnserver
LOCK=/var/lock/subsys/vpnserver
test -x $DAEMON || exit 0
case "$1" in
start)
$DAEMON start
touch $LOCK
;;
stop)
$DAEMON stop
rm $LOCK
;;
restart)
$DAEMON stop
sleep 3
$DAEMON start
;;
*)
echo "Usage: $0 {start|stop|restart}"
exit 1
esac
exit 0' >> $SOFTETHER_INSTALLER_ROOT/vpnserver

mv $SOFTETHER_INSTALLER_ROOT/vpnserver $INITD_FILE

chmod 755 $INITD_FILE

/sbin/chkconfig --add vpnserver

$INITD_FILE start 

rm -rf $SOFTETHER_INSTALLER_ROOT