#!/bin/sh

yum -y install wget

FILE=/etc/yum.repos.d/webmin.repo
if [ -f $FILE ]; then
  rm $FILE
fi

touch /etc/yum.repos.d/webmin.repo

echo '[Webmin]
name=Webmin Distribution Neutral
#baseurl=https://download.webmin.com/download/yum
mirrorlist=https://download.webmin.com/download/yum/mirrorlist
enabled=1' >> /etc/yum.repos.d/webmin.repo

wget http://www.webmin.com/jcameron-key.asc

rpm --import jcameron-key.asc

yum install -y webmin

sed -i 's/ssl=1/ssl=0/g' /etc/webmin/miniserv.conf

/etc/init.d/webmin restart

# firewall-cmd --zone=public --add-port=10000/tcp --permanent
# firewall-cmd --reload