#!/bin/sh

SCRIPT_ROOT=$PWD

show_welcome() {
    whiptail --title "SETUP OS CENTOS DEVELOPMENT SERVER" --msgbox "Welcome on the setup tool for centos development server" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH
    show_menu_main
}

show_credits() {
    whiptail --title "Credits" --msgbox "Real-IT \n Loïc BERTRAND \n Paul JACOBS" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH
    show_menu_main
}

show_menu_post_install() {
    local CHOICE=$(whiptail --notags --cancel-button "Back" --title "Post install menu" --menu "" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH 4 \
        "1" "Install sysadmin tools" \
        "2" "Install webmin" \
        "3" "Install docker services" \
        3>&1 1>&2 2>&3)
    case $CHOICE in
        0)
            show_menu_main
        ;;
        1)  
            clear
            # sh -c "$(curl -fsSL $SCRIPT_SYADMIN_TOOL)"
            whiptail --title "Success" --msgbox "Installation successfully completed." $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH
            show_menu_post_install
        ;;
        2)  
            clear
            # sh -c "$(curl -fsSL $SCRIPT_WEBMIN)"
            whiptail --title "Success" --msgbox "Installation successfully completed." $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH
            show_menu_post_install
        ;;
        3)
            clear
            # sh -c "$(curl -fsSL $SCRIPT_DOCKER)"
            whiptail --title "Success" --msgbox "Installation successfully completed." $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH
            show_menu_post_install
            
        ;;
        *)
            show_menu_main
        ;;
    esac
}

show_menu_main() {
    local CHOICE=$(whiptail --notags --cancel-button "Exit" --title "Main menu" --menu "" $WHIPTAIL_HEIGTH $WHIPTAIL_WIDTH 4 \
        "1" "Post install tools" \
        "2" "Credits" \
        3>&1 1>&2 2>&3)
    case $CHOICE in
        1)
            show_menu_post_install
            # whiptail --title "Option 1" --msgbox "You chose option 1. Exit status $?" 8 45
        ;;
        2)
            show_credits
        ;;
        *)
            exit
        ;;
    esac
}

ROOT_SCRIPT_PATH=""
SCRIPT_SYADMIN_TOOL="${ROOT_SCRIPT_PATH}/script/sysadmin_tool-install.sh"
SCRIPT_WEBMIN="${ROOT_SCRIPT_PATH}/script/webmin-install.sh"
SCRIPT_DOCKER="${ROOT_SCRIPT_PATH}/script/docker_services-setup.sh"

WHIPTAIL_HEIGTH=15
WHIPTAIL_WIDTH=60

show_welcome