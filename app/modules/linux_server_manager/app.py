import app.lib.DialogAIO as DialogAIO
import app.lib.system_administration_aio.system_informations_aio as system_informations_aio
import app.resources.constants as constants
import app.lib.system_administration_aio.shell_command_aio as shell_command_aio
import app.modules.linux_server_manager.resources.constants as module_constants
import app.lib.system_administration_aio.setup as setup


def start():
    dialog_aio = DialogAIO.DialogAIO().get_instance()
    dialog_aio.set_module_name(module_constants.dialog.MODULE)
    while True:
        action = dialog_aio.choose_in_menu(module_constants.dialog.MENU_MODULE)
        if action == 1:
            process_administrator_configuration()
        elif action is None:
            return


def process_administrator_configuration():
    dialog_aio = DialogAIO.DialogAIO().get_instance()
    while True:
        user = dialog_aio.input_text(module_constants.dialog.INPUTBOX_USER_CONFIGURATION)
        if user != "":
            break
        dialog_aio.show_message(("", "Please input a valid username"))

    if system_informations_aio.does_user_exists(user):
        if not dialog_aio.ask_yes_no(("", user + " exists, do you want to configure her/him ?", "Yes, configure", "No, cancel")):
            return
    else:
        if dialog_aio.ask_yes_no(("", user + " does not exist, do you want to add and configure her/him ?", "Yes, add and configure", "No, cancel")):
            password = dialog_aio.input_password(("", "Input a not empty password for the new user", ""))
            shell_command_aio.execute_shell("adduser --password=" + password)
            dialog_aio.show_message(("", "The user" + user + " has been created."))
            return
    setup.fonts.setup_fonts(user, constants.path_config.FONTS)
    setup.zsh.setup_zsh(user, constants.path_config.ZSH)
    dialog_aio.show_message(("", "User configuration done !"))


def process_security_configuration():
    setup.security.disable_ssh_login_as_root()


def __add_new_user():
    print()
