import app.app as app
import app.test.test as test
import sys

if __name__ == '__main__':
    if len(sys.argv) == 1:
        app.start()
    elif len(sys.argv) == 2:
        if sys.argv[1] == "test":
            test.start()
