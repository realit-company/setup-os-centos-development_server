import signal
import app.lib.DialogAIO as DialogAIO
import app.lib.TerminalAIO as TerminalAIO
import app.lib.system_administration_aio.system_informations_aio as system_informations_aio
import app.resources.constants as constants
import app.modules.linux_server_manager.app as linux_server_manager


def start():
    dialog_aio = DialogAIO.DialogAIO().get_instance()
    dialog_aio.set_title(constants.dialog.TITLE)
    if not system_informations_aio.is_current_user_root():
        dialog_aio.show_message(constants.dialog.MESSAGEBOX_NO_ROOT)
        end()
    dialog_aio.show_message(constants.dialog.MESSAGEBOX_WELCOME)
    while True:
        action = dialog_aio.choose_in_menu(constants.dialog.MENU_LAUNCHER)
        if action == 1:
            linux_server_manager.start()
        elif action == 2:
            pass
        elif action == 3:
            pass
        elif action is None:
            break
    end()


def end():
    TerminalAIO.clean()
    TerminalAIO.simple_print(constants.dialog.MESSAGE_END)
    exit(0)


signal.signal(signal.SIGINT, end)
