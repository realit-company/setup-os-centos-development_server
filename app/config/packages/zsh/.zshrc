conf_file=($HOME/.zsh/*.zsh(N))

for conf in $conf_file; do
    source ${conf}
done

# If rbenv is present, configure it for use
if which rbenv &> /dev/null; then
    # Put the rbenv entry at the front of the line
    path=($HOME/.rbenv/bin $path)
    # enable shims and auto-completion
    eval "$(rbenv init -)"
fi

if [ -f "$HOME/.rvm/script/rvm" ]; then
    source "$HOME/.rvm/script/rvm"
fi


path=($HOME/.bin $path)

setopt no_share_history

TERM=xterm

figlet -Wtc Real - IT
