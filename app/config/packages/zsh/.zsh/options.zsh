setopt no_beep
setopt interactive_comments

setopt auto_cd
setopt auto_pushd
setopt pushd_to_home
setopt pushd_ignore_dups
setopt pushd_silent

setopt extended_glob

setopt always_to_end
setopt auto_menu
unsetopt menu_complete
setopt auto_name_dirs
setopt complete_in_word

setopt correct
setopt correctall

setopt promptsubst
setopt transient_rprompt

setopt multios
