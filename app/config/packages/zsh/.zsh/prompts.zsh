#{{{ Load color capabilities
autoload -U colors && colors
#}}}
#{{{ Load prompt hook capabilities
autoload -Uz add-zsh-hook
#}}}

#{{{ Useful function for prompt
function virtualenv_info {
    [ $VIRTUAL_ENV ] && echo "(%{$fg[green]%}🐍 $(basename $VIRTUAL_ENV)%{$reset_color%})"
}

function prompt_char {
    typeset -g scm=''
    which git &> /dev/null && git branch >/dev/null 2>/dev/null && scm='git' && return
    which hg &> /dev/null && hg root >/dev/null 2>/dev/null && scm='hg' && return
    which svn &> /dev/null && svn info >/dev/null 2>/dev/null && scm='svn' && return
}
add-zsh-hook precmd prompt_char

function show_prompt_char {
    case $scm in
    'git')
        echo '±'
        ;;
    'hg')
        echo '☿'
        ;;
    'svn')
        echo '$'
        ;;
    *)
        echo "%#"
        ;;
    esac
}

# determine Ruby version whether using RVM or rbenv
function _update_ruby_version() {
    typeset -g ruby_version=''
    local version=''

    if which rvm-prompt &> /dev/null && [ -n "$rvm_ruby_string" ]; then
        version="$(rvm-prompt v g)"
    else
        if which rbenv &> /dev/null; then
            version="$(rbenv version | sed -e "s/ (set.*$//")"
            if [[ "$version" = "system" ]]; then
                version=''
            fi
        fi
    fi
    if [ -n "$version" ]; then
        ruby_version="(%{$fg[red]%}💎 ${version}%{$reset_color%})"
    fi
}
add-zsh-hook precmd _update_ruby_version

function current_pwd() {
    echo $(pwd | sed -e "s,^$HOME,~,")
}

process_status()
{
    local ex=$?
    typeset -g psvar

    if [[ $ex -ge 128 ]]; then
        sig=$signals[$ex-127]
        psvar="%{$fg[cyan]%}sig${(L)sig}%{$reset_color%}"
    elif [[ $ex -eq 0 ]]; then
        psvar="%{$fg[green]%}$ex%{$reset_color%}"
    else
        psvar="%{$fg[red]%}error: $ex%{$reset_color%}"
    fi
}
add-zsh-hook precmd process_status

#}}}

#{{{ Generic CVS variable
PROMPT_PREFIX="%{$fg[green]%}[%{$reset_color%}"
PROMPT_SUFFIX="%{$fg[green]%}]%{$reset_color%}"
PROMPT_MERGING="%{$fg_bold[magenta]%}⚡︎%{$reset_color%}"
PROMPT_UNTRACKED="%{$fg_bold[red]%}⚠%{$reset_color%}"
PROMPT_MODIFIED="%{$fg_bold[yellow]%}±%{$reset_color%}"
PROMPT_STAGED="%{$fg_bold[green]%}✓%{$reset_color%}"
#}}}

#{{{ Specific Git functions
GIT_PROMPT_AHEAD="%{$fg[red]%}⤷NUM%{$reset_color%}"
GIT_PROMPT_BEHIND="%{$fg[cyan]%}NUM⤶%{$reset_color%}"

# Show Git branch/tag, or name-rev if on detached head
function parse_git_branch() {
    (git symbolic-ref -q HEAD || git name-rev --name-only --no-undefined --always HEAD) 2> /dev/null
}

# Show different symbols as appropriate for various Git repository states
function parse_git_state() {
    local GIT_STATE=""

    local NUM_BEHIND="$(git log --oneline ..@{u} 2> /dev/null | wc -l | tr -d ' ')"
    if [ "$NUM_BEHIND" -gt 0 ]; then
        GIT_STATE=$GIT_STATE${GIT_PROMPT_BEHIND//NUM/$NUM_BEHIND}
    fi

    local NUM_AHEAD="$(git log --oneline @{u}.. 2> /dev/null | wc -l | tr -d ' ')"
    if [ "$NUM_AHEAD" -gt 0 ]; then
        GIT_STATE=$GIT_STATE${GIT_PROMPT_AHEAD//NUM/$NUM_AHEAD}
    fi

    local GIT_DIR="$(git rev-parse --git-dir 2> /dev/null)"
    if [ -n $GIT_DIR ] && test -r $GIT_DIR/MERGE_HEAD; then
        GIT_STATE=$GIT_STATE$PROMPT_MERGING
    fi

    if [[ -n $(git ls-files --other --exclude-standard 2> /dev/null) ]]; then
        GIT_STATE=$GIT_STATE$PROMPT_UNTRACKED
    fi

    if ! git diff --quiet 2> /dev/null; then
        GIT_STATE=$GIT_STATE$PROMPT_MODIFIED
    fi

    if ! git diff --cached --quiet 2> /dev/null; then
        GIT_STATE=$GIT_STATE$PROMPT_STAGED
    fi

    if [[ -n $GIT_STATE ]]; then
        echo " $GIT_STATE"
    fi
}

# If inside a Git repository, print its branch and state
function git_prompt_string() {
    local git_where="$(parse_git_branch)"
    [ -n "$git_where" ] && echo "$PROMPT_PREFIX%{$fg[magenta]%}ᛘ${git_where#(refs/heads/|tags/)}$(parse_git_state)%{$reset_color%}$PROMPT_SUFFIX"
}

#}}}

#{{{ Specific SVN function
function parse_svn_branch() {
    local rev="$(svn info --show-item revision --no-newline | tr -d ' ')"
    local branch="$(svn info --show-item relative-url --no-newline)"
    case "$branch" in
    */trunk)
            branch='trunk'
            ;;
    */branches/*)
            branch="$(echo ${branch} | sed 's|.*/branches/||')"
            ;;
    */tags/*)
            branch="tag:$(echo ${branch} | sed 's|.*/tags/||')"
            ;;
    esac
    echo "$branch:$rev"
}

function parse_svn_state() {
    local SVN_STATE=""

    local TAGLIST="$(svn st | cut -f1 -d' ' | sort -u)"
    for tag in $(svn st | cut -f1 -d' ' | sort -u); do
        case $tag in
        '?'|'!')
            SVN_STATE=$SVN_STATE$PROMPT_UNTRACKED
            ;;
        'M')
            SVN_STATE=$SVN_STATE$PROMPT_MODIFIED
            ;;
        'A'|'D'|'R')
            SVN_STATE=$SVN_STATE$PROMPT_STAGED
            ;;
        'C'|'~')
            SVN_STATE=$SVN_STATE$PROMPT_MERGING
            ;;
        esac
    done

    if [[ -n $SVN_STATE ]]; then
        echo " $SVN_STATE"
    fi
}

function svn_prompt_string() {
    local svn_where="$(parse_svn_branch)"
    [ -n "$svn_where" ] && echo "$PROMPT_PREFIX%{$fg[magenta]%}ᛘ${svn_where}$(parse_svn_state)%{$reset_color%}$PROMPT_SUFFIX"
}
#}}}

#{{{ SCM generic function
function prompt_string() {
    case ${scm} in
    'git')
            git_prompt_string
            ;;
    'svn')
            svn_prompt_string
            ;;
    esac
}
#}}}

#{{{ Prompt variable definition
#export RPROMPT='${PR_GREEN}$(virtualenv_info)%{$reset_color%} ${PR_RED}${ruby_version}%{$reset_color%}'

PROMPT=""
PROMPT+="[%{$fg[green]%}%n%{$reset_color%}@"
PROMPT+="%{$fg[blue]%}%m%{$reset_color%}:"
PROMPT+="%{$fg[yellow]%}%~%{$reset_color%}:"
PROMPT+="%{$fg[cyan]%}%*%{$reset_color%}]"
PROMPT+="%{$reset_color%}\$(prompt_string)\$(show_prompt_char)"

export PROMPT

SPROMPT=""
SPROMPT="Correct %{$fg[red]%}%R%{$reset_color%} to %{$fg[green]%}%r%{$reset_color%} [(y)es (n)o (a)bort (e)dit]? "

export SPROMPT

RPROMPT=""
RPROMPT+="\$psvar"
RPROMPT+="%{$fg[green]%}\$(virtualenv_info)%{$reset_color%}"
RPROMPT+="\${ruby_version}"

export RPROMPT

#}}}

