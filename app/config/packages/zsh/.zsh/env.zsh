# export MANPAGER="/bin/sh -c \"sed -e 's/.$(echo -e '\010')//g' \
#                 | vim -R -c 'set ft=man nomod nolist' -c 'set nonu' -\""
export WORDCHARS=""
export EDITOR=/usr/bin/vim
export BROWSER=/usr/bin/google-chrome-stable
export VISUAL=/usr/bin/vim
export MANPAGER=/usr/bin/less