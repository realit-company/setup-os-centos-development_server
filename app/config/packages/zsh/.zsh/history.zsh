HISTSIZE=10000
SAVEHIST=9000
HISTFILE=~/.zsh/history.bak

setopt extended_history
setopt inc_append_history
setopt hist_expire_dups_first
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_find_no_dups
setopt hist_reduce_blanks
#setopt hist_verify
