alias ls='lsd --color=auto'
alias grep='grep --color=auto --exclude-dir=".svn" --exclude-dir=".git" -n'
alias egrep='egrep --color=auto'
alias gl='git log --graph --pretty=format:"%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset" --abbrev-commit'
