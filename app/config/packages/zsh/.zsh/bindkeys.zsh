# Setup key
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line

bindkey "^[[A" history-search-backward
bindkey "^[[B" history-search-forward

bindkey "^[[3~" delete-char
bindkey "^[[2~" overwrite-mode

bindkey "^[[Z" reverse-menu-complete

bindkey "^H" backward-delete-word

bindkey "^[[1;5D" backward-word
bindkey "^[[1;5C" forward-word
