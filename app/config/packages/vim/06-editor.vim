" enable automatic indentation
set autoindent

" enable smart indentation
set smartindent

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

" indent to 4 spaces
set expandtab
set tabstop=4
set shiftwidth=4

function! Preserve(command)
    " Preparation: save last search and cursor position
    let l:winview = winsaveview()
    let l:old_query = getreg('/')
    execute 'keepjumps ' . a:command
    call winrestview(l:winview)
    call setreg('/', l:old_query)
endfunction

nmap <F5> :call Preserve('%s/[ \t]*$//')<CR>
