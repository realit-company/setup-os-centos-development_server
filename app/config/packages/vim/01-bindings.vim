" Remap moving key
noremap c h
noremap r l
noremap t j
noremap s k
" Remap arrow Up and Down to gj and gk to naturally move over wrapped lines
noremap <Up> gk
noremap <Down> gj

" Remap screen moving
noremap C H
noremap R L

" Join lines
noremap T J

" Man of word under cursor
noremap S K

" Replace mode
noremap h r
noremap H R

" Map éÉ to wW key for word forward
noremap é w
noremap É W

" Direct access to < and >
noremap « <
noremap » >
" Enhance indent/deindent in visual mode: no need to reselect block
vnoremap « <gv
vnoremap » >gv

" Direct access to windows moving with <ALT>+ctsr
noremap © <C-W><Left>
noremap þ <C-W><Down>
noremap ß <C-W><Up>
noremap ® <C-W><Right>

" Resize current window
noremap ſ <C-W><
noremap Þ <C-W>-
noremap ẞ <C-W>+
noremap ™ <C-W>>
noremap º <C-W>=

" Close current window without quitting vim
noremap q <C-W>c

" Save current buffer with <C-s>
noremap <C-s> :w<Return>
inoremap <C-s> <ESC>:w<Return>i

" Change leader key to "," (more accessible than \)
let mapleader = ","
