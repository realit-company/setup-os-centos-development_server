" Open NERDTree automatically when vim starts up if no file specified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

" Map NERDTree to F2 key
map <F2> :NERDTreeToggle<CR>
" Show current file in NERDTree
map " :NERDTreeFind<CR>

" Close vim if NERDTree is the last open buffer
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif


" If a file node is selected, it is opened in the previous window.
" If a directory is selected it is opened or closed depending on its current
" state.
" If a bookmark that links to a directory is selected then that directory
" becomes the new root.
" If a bookmark that links to a file is selected then that file is opened in
" the previous window.
let NERDTreeMapActivateNode='o'
" Opens the selected file in a new tab. If a directory is selected, a fresh
" NERD Tree for that directory is opened in a new tab.
" If a bookmark which points to a directory is selected, open a NERD tree for
" that directory in a new tab. If the bookmark points to a file, open that file
" in a new tab.
let NERDTreeMapOpenInTab='é'
let NERDTreeMapOpenInTabSilent='É'
" Opens the selected file in a new split window and puts the cursor in the new
" window.
let NERDTreeMapOpenSplit='h'
" Opens the selected file in a new vertically split window and puts the cursor
" in the new window.
let NERDTreeMapOpenVSplit='v'
" Recursively opens the selected directory.
let NERDTreeMapOpenRecursively='O'
" Closes the parent of the selected node.
let NERDTreeMapCloseDir='x'
let NERDTreeMapCloseChildren='X'
" |:edit|s the selected directory, or the selected file's directory. This could
" result in a NERD tree or a netrw being opened, depending on
let NERDTreeMapOpenExpl='e'
" Deletes the currently selected bookmark.
let NERDTreeMapDeleteBookmark='D'
" Jump to the tree root.
let NERDTreeMapJumpRoot='P'
" Jump to the parent node of the selected node.
let NERDTreeMapJumpParent='p'
" Jump to the first child of the current nodes parent.
" If the cursor is already on the first node then do the following:
"     * loop back thru the siblings of the current nodes parent until we find
"       an open dir with children
"     * go to the first child of that node
let NERDTreeMapJumpFirstChild='B'
" Jump to the last child of the current nodes parent.
" If the cursor is already on the last node then do the following:
"     * loop forward thru the siblings of the current nodes parent until we
"       find an open dir with children
"     * go to the last child of that node
let NERDTreeMapJumpLastChild='b'
" Jump to the next sibling of the selected node.
let NERDTreeMapJumpNextSibling='<C-T>'
" Jump to the previous sibling of the selected node.
let NERDTreeMapJumpPrevSibling='<C-S>'
" Make the selected directory node the new tree root. If a file is selected,
" its parent is used.
let NERDTreeMapChangeRoot='C'
" Move the tree root up a dir (like doing a "cd ..").
let NERDTreeMapUpdir='u'
" Like |NERDTree-u| except that the old tree root is kept open.
let NERDTreeMapUpdirKeepOpen='U'
" If a dir is selected, recursively refresh that dir, i.e. scan the filesystem
" for changes and represent them in the tree.
" If a file node is selected then the above is done on it's parent.
let NERDTreeMapRefresh='r'
" Recursively refresh the tree root.
let NERDTreeMapRefreshRoot='R'
" Display the NERD tree menu. See |NERDTreeMenu| for details.
let NERDTreeMapMenu='m'
" Change vims current working directory to that of the selected node.
let NERDTreeMapChdir='cd'
" Change tree root to vims current working directory.
let NERDTreeMapCWD='CD'
" Toggles whether hidden files (i.e. "dot files") are displayed.
let NERDTreeMapToggleHidden='<C-h>'
" Toggles whether file filters are used. See |'NERDTreeIgnore'| for details.
let NERDTreeMapToggleFilters='f'
" Toggles whether file nodes are displayed.
let NERDTreeMapToggleFiles='F'
" Toggles whether the bookmarks table is displayed.
let NERDTreeMapToggleBookmarks='a'
" Closes the NERDtree window.
let NERDTreeMapQuit='q'
" Maximize (zoom) and minimize the NERDtree window.
let NERDTreeMapToggleZoom='A'
" Toggles whether the quickhelp is displayed.
let NERDTreeMapHelp='?'


" Automatically change Vim's current directory when changing NERDTree root.
let NERDTreeChDirMode=2
" List file ignored by NERDTree
let NERDTreeIgnore=['\.git$', '\.svn$', '\.o$[[file]]', '\.d$[[dir]]', '__pycache__$[[dir]]']
" Respect wildignore settings
let NERDTreeRespectWildIgnore=1
" Bookmark path
let NERDTreeBookmarksFile='$HOME/.vim/bookmarks'
" Single click to open/close folder but double click to open file
let NERDTreeMouseMode=2
" Show bookmarks by default
let NERDTreeShowBookmarks=1

" Show ignored status
let g:NERDTreeShowIgnoredStatus = 1
