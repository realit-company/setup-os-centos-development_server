" enable file type detection
filetype on

" enable loading the plugin for appropriate file
filetype plugin on

" resync all syntax highlighting
au BufEnter * :syntax sync fromstart

" set path to current directory and all directory below
" so :find and :sfind is easier to use on large project
set path=$PWD/**
