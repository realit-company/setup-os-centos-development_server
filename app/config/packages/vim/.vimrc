" Avoid compatibility with old VI
set nocompatible

" source template
" source /usr/share/vim/vim81/defaults.vim

" Load settings in config folder
runtime! config/**/*.vim

map <F1> :SyntasticCheck<CR>

set list listchars=nbsp:¤,tab:··,trail:¤,extends:▶,precedes:◀,eol:¶

set colorcolumn=80

