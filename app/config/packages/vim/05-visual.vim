" show matching paren
set showmatch

" show line number
set number

" no wrap line
set nowrap

" set color scheme
"set t_Co=256
set background=dark
"let g:hybrid_use_Xresources = 1
colorscheme elflord

if has("gui_running")
    set guifont=Source\ Code\ Pro\ 10
    colorscheme torte
endif

" enable wildmenu
set wildmenu

" highlight current line
set ruler

" allow buffer change without saving
set hid

" increase speed by do not redraw screen while exec macro
set lazyredraw

" enable syntax coloration
syntax on

" show tabs, trailing space
set list listchars=nbsp:¤,tab:··,trail:¤,extends:▶,precedes:◀,eol:¶

" Show a  colored column at 80 to avoid going to far to the right
set colorcolumn=80

" always show status bar
set laststatus=2

" enable fold
set foldmethod=indent

"set fold level
:set foldlevel=10
