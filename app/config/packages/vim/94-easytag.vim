let g:easytags_cmd = expand("$HOME/.vim/vim-ctags")
" Enable background tag update
let g:easytags_async = 1

" Allow dynamic tag files, if .tags file exists in project
set tags=./.tags;~/.vim/tags
let g:easytags_dynamic_files = 1

" Recursively scan for all tags
let g:easytags_autorecurse = 1

" Tag members of class and struct
let g:easytags_include_members = 1

let g:easytags_always_enabled = 0
let g:easytags_auto_highlight = 0
let g:easytags_auto_update = 0
