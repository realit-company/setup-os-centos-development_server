" Disable terminal flow control to allow C-S and C-Q key binding
silent exec "!stty -ixon"
