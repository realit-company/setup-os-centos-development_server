let g:lightline = {
    \ 'active': {
    \   'left': [ [ 'mode', 'paste' ],
    \             [ 'fugitive', 'readonly', 'filename', 'modified' ] ]
    \ },
    \ 'component': {
    \   'readonly': '%{&readonly?"\xe2\x9a\xbf":""}',
    \   'modified': '%{&modified?"\u270e":""}',
    \   'fugitive': '%{exists("*fugitive#head")?"\xe1\x9b\x98".fugitive#head():""}',
    \ },
    \ 'component_visible_condition': {
    \   'fugitive': '(exists("*fugitive#head") && ""!=fugitive#head())'
    \ },
    \}
