function s:CustomizeYcmQuickFixWindow()
    " Move the window at the bottom of the screen.
    execute "wincmd J"
endfunction

autocmd User YcmQuickFixOpened call s:CustomizeYcmQuickFixWindow()

" Do not ask if ycm_extra_conf.py must be loaded in home code folder
let g:ycm_extra_conf_globlist = [ '~/Code/*' ]
