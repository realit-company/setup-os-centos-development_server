" Incremental search
set incsearch

" highlight search matches
set hlsearch

" smart case search: if there is upper case character in the search pattern,
" search is case sensitive.
set smartcase

" cancel current search with <ESC> key
nnoremap <silent> <Esc> :nohlsearch<Bar>:echo<Cr>
" needed so that vim still understands escape sequences
nnoremap <Esc>^[ <Esc>^[
