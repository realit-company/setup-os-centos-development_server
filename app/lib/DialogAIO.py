from dialog import Dialog


class DialogAIO:

    __instance = None

    def get_instance(self):
        if not DialogAIO.__instance:
            DialogAIO.__instance = DialogAIO.__DialogAIO()
        return self.__instance

    class __DialogAIO:
        def __init__(self):
            self.__dialog = Dialog(autowidgetsize=True)
            self.__title = ""
            self.__module_name = ""

        def set_title(self, title):
            self.__title = title
            self.set_dialog_full_title()

        def set_module_name(self, module_name):
            self.__module_name = module_name
            self.set_dialog_full_title()

        def set_dialog_full_title(self, subtitle=""):
            dialo_full_title = ""
            if self.__module_name != "":
                dialo_full_title += self.__module_name + " "
            if self.__title != "":
                dialo_full_title += "[" + self.__title + "]"
            if subtitle != "":
                dialo_full_title += " - " + subtitle
            self.__dialog.set_background_title(dialo_full_title)

        def show_message(self, data):
            (subtitle, text) = data
            self.set_dialog_full_title(subtitle)
            self.__dialog.msgbox(text)
            self.set_dialog_full_title()

        def choose_in_menu(self, data):
            (subtitle, text, choices) = data
            self.set_dialog_full_title(subtitle=subtitle)
            (code, tag) = self.__dialog.menu(text, choices=choices)
            self.set_dialog_full_title()
            if code == Dialog.CANCEL:
                return None
            if code == Dialog.OK:
                for choice_number in range(len(choices)):
                    if tag == choices[choice_number][0]:
                        return choice_number + 1

        def ask_yes_no(self, data):
            (subtitle, text, yes_label, no_label) = data
            self.set_dialog_full_title(subtitle=subtitle)
            code = self.__dialog.yesno(text, yes_label=yes_label, no_label=no_label)
            self.set_dialog_full_title()
            return code == Dialog.OK

        def input_text(self, data):
            (subtitle, text, default_text) = data
            self.set_dialog_full_title(subtitle=subtitle)
            (code, inputed_text) = self.__dialog.inputbox(text, init=default_text)
            self.set_dialog_full_title()
            if code == Dialog.CANCEL:
                return None
            if code == Dialog.OK:
                return inputed_text

        def input_password(self, data):
            (subtitle, text, default_password) = data
            self.set_dialog_full_title(subtitle=subtitle)
            while True:
                (code, password) = self.__dialog.passwordbox(text, init=default_password, insecure=False)
                if code == Dialog.CANCEL:
                    self.set_dialog_full_title()
                    return None
                if password == "":
                    self.show_message(("", "Please input a password."))
                    continue
                (code, password_confirmation) = self.__dialog.passwordbox(text + "\n\nPlease input the same string to confirm", init=default_password, insecure=False)
                if code == Dialog.CANCEL:
                    continue
                if password_confirmation == password:
                    self.set_dialog_full_title()
                    return password
                else:
                    self.show_message(("", "Inputs do not match. Please try again.\n\n(Check your Caps Lock / Num Lock)"))
