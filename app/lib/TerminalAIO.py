import os


def clean():
    os.system("cls" if os.name == "nt" else "clear")


def simple_print(text):
    print(text, end='')


def println(text):
    print(text)


class TerminalAIO:

    __instance = None

    def get_instance(self):
        if not TerminalAIO.__instance:
            TerminalAIO.__instance = TerminalAIO.__TerminalAIO()
        return self.__instance

    class __TerminalAIO:
        def __init__(self):
            self.title = ""

        def set_titles(self, *args):
            if len(args) == 1:
                self.title = args[0]
            elif len(args) == 2:
                self.title = args[0] + " - " + args[1]
            else:
                raise Exception("you must pass 1 string (title) or two strings (title, subtitles)")
