import os
import shutil
import subprocess
import re
import sys


def does_file_exist(path):
    return os.path.isfile(path)


def does_directory_exist(path):
    return os.path.isdir(path)


def copy_file(path_source, path_destination):
    shutil.copyfile(path_source, path_destination)


def copy_directory(path_source, path_destination):
    for item in os.listdir(path_source):
        source = os.path.join(path_source, item)
        destination = os.path.join(path_destination, item)
        if os.path.isdir(source):
            shutil.copytree(source, destination)
        else:
            shutil.copy2(source, destination)


def move_file(path_source, path_destination):
    shutil.move(path_source, path_destination)


def move_directory(path_source, path_destination):
    shutil.move(path_source, path_destination)


def remove_file(path):
    try:
        os.remove(path)
    except FileNotFoundError:
        pass


def remove_directory(path):
    shutil.rmtree(path, ignore_errors=True)


def get_root_package_name():
    return re.findall(r'\w+', __name__)[0]


def get_working_directory():
    return os.getcwd()


def get_root_package_path():
    return get_working_directory() + "/" + get_root_package_name()


def make_directory(path):
    try:
        os.mkdir(path)
    except FileExistsError:
        pass


def get_user_home_path(user):
    home_user = os.path.expanduser("~" + user)
    if home_user == "~" + user:
        return None
    else:
        return home_user