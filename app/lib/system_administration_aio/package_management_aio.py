import app.lib.system_administration_aio.shell_command_aio as shell_command_aio
import app.lib.system_administration_aio.system_informations_aio as system_informations_aio

import os
import shutil
import subprocess
import re
import sys


def is_package_installed(package):
    return not ("which: no" in shell_command_aio.query_shell("which " + package)[0])


def install_package(package, package_manager):
    if package_manager == "yay":
        shell_command_aio.execute_shell("yay -S " + package + " --noconfirm --needed")

    if package_manager == "yum":
        shell_command_aio.execute_shell("yum -y install " + package)

    if package_manager == "apt":
        shell_command_aio.execute_shell("apt-get install -y " + package)

    if package_manager == "snap":
        shell_command_aio.execute_shell("snap install " + package)


def get_packages_list_for_current_os(brut_package_structure):
    try:
        return brut_package_structure[system_informations_aio.get_distro()]
    except KeyError:
        return None


def install_not_installed_packages(brut_package_structure):
    packages_to_install = get_packages_list_for_current_os(brut_package_structure)
    for package, package_manager in packages_to_install.items():
        install_package(package, package_manager)
