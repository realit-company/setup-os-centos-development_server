import subprocess


def execute_shell(command):
    subprocess.run(command.split(' '), stdout=subprocess.DEVNULL)


def query_shell(command):
    returned_lines = []
    popen = subprocess.Popen(command.split(' '), stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True)
    for line in iter(popen.stdout.readline, ""):
        returned_lines.append(line.replace('\n', ''))
    for line in iter(popen.stderr.readline, ""):
        returned_lines.append(line.replace('\n', ''))
    return returned_lines