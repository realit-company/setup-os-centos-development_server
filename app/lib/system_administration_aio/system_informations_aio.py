import re
import os
import pwd
import grp


def is_current_user_root():
    return os.geteuid() == 0


def get_distro():
    distro = ""
    with open("/etc/os-release") as lines:
        for line in lines:
            if re.match(r'ID=', line):
                distro = re.findall(r'[^"(ID=)]+', line)[0].replace('\n', '')
                break
    distro = distro
    if distro == "centos":
        return "centos"
    elif distro == "ubuntu" or distro == "debian":
        return "debian"
    elif distro == "arch" or distro == "manjaro":
        return "arch"
    else:
        return None


def does_user_exists(user):
    try:
        pwd.getpwnam(user)
    except KeyError:
        return False
    return True


def does_group_exists(group):
    try:
        grp.getgrnam(group)
    except KeyError:
        return False
    return True
