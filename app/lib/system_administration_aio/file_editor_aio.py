import re
import fileinput
import shutil
import tempfile
import os


def does_file_contain_string(path_file, string):
    with open(path_file) as lines:
        for line in lines:
            if string in line:
                return True
    return False


def write_string_on_new_line(path_file, string):
    open(path_file, 'w').write(string+"\n")


def find_and_replace_line(path_file, pattern, new_line):
    for line in fileinput.FileInput(path_file, inplace=True):
        if pattern in line:
            print(new_line, end='\n')
        else:
            print(line, end='')