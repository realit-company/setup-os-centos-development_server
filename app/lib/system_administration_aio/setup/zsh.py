import app.lib.system_administration_aio.filesystem_aio as filesystem_aio
import app.lib.system_administration_aio.package_management_aio as package_management_aio
import app.lib.system_administration_aio.shell_command_aio as shell_command_aio


def setup_zsh(user, path_resource_directory):
    path_user_home = filesystem_aio.get_user_home_path(user)
    packages_to_install = {
        'centos': {
            'zsh': 'yum',
            'lsd': 'snap',
            'figlet': 'yum'
        },
        'debian': {
            'zsh': 'apt',
            'lsd': 'apt',
            'figlet': 'apt'
        },
        'arch': {
            'zsh': 'yay',
            'lsd': 'yay',
            'figlet': 'yay'
        },
    }
    package_management_aio.install_not_installed_packages(packages_to_install)
    filesystem_aio.remove_directory(path_user_home + "/.zsh")
    filesystem_aio.remove_file(path_user_home + "/.zshrc")
    filesystem_aio.copy_directory(path_resource_directory, path_user_home)
    shell_command_aio.execute_shell("usermod --shell /bin/zsh " + user)
