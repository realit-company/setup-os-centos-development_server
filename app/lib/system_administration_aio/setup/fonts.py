import app.lib.system_administration_aio.filesystem_aio as filesystem_aio


def setup_fonts(user, path_resource_directory):
    path_user_home = filesystem_aio.get_user_home_path(user)
    filesystem_aio.make_directory(path_user_home + "/.fonts")
    filesystem_aio.copy_directory(path_resource_directory, path_user_home + "/.fonts")
