import app.lib.system_administration_aio.file_editor_aio as file_editor_aio


def disable_ssh_login_as_root():
    file_editor_aio.find_and_replace_line("/etc/ssh/sshd_config", "PermitRootLogin", "PermitRootLogin no")
