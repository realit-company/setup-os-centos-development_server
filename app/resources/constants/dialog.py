TITLE = "Linux Tool Suite"

LABEL = "Real-IT"

AUTHOR = "Real-IT"

SUPPORTED_OS = "Centos 7, Ubuntu 18.04 LTS and Archlinux-based OS"

MESSAGE_END = "Goodbye ! Thank you for using " + TITLE + " by " + AUTHOR + " !\n\n"

MESSAGEBOX_WELCOME = (
    "Welcome !",
    "Welcome to " + TITLE + " by " + AUTHOR + "\n\nIt currently supports " + SUPPORTED_OS + "\n\nPowered by Python 3"
)

MESSAGEBOX_NO_ROOT = ("", "You must run " + TITLE + " as root.")

MENU_LAUNCHER = ("Launcher", "Launcher Menu:",
                 [("1", "Linux Server Manager"),
                  ("2", "Linux Workstation Manager"),
                  ("3", "Linux Docker Services Manager")])
