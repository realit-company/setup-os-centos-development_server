import app.lib.system_administration_aio.file_editor_aio as file_editor_aio
import os


def start():
    path_file = os.getcwd() + "/test/sample"

    file_editor_aio.find_and_replace_line(path_file, "PermitRootLogin", "PermitRootLogin no")
