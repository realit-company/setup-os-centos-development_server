help:
	@echo "Usage:"
	@echo "	install		: setup a venv and install all dependencies into it"
	@echo "	run		: start program"
	@echo "	test		: start tests"

install:
	@exec ./setup/driver
	@pip3 install virtualenv --user
	@pip3 install pipreqs --user
	@python3 -m virtualenv .venv
	@source ./.venv/bin/activate; python3 -m pip install -r ./app/requirements.txt

run:
	@export TERM=xterm; source ./.venv/bin/activate; python3 -m app

test:
	@export TERM=xterm; source ./.venv/bin/activate; python3 -m app test

deploy:
	@rm -rf /root/setup-server
	@cp -r /mnt/setup-server /root/setup-server
	@cd /root/setup-server
	@make install
	@make run
	@cd -